// Fill out your copyright notice in the Description page of Project Settings.


#include "Slowdown.h"
#include "SnakeBase.h"

// Sets default values
ASlowdown::ASlowdown()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASlowdown::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASlowdown::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASlowdown::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SlowdownMove();
		}
	}

	if (Cast<ASnakeBase>(Interactor) != nullptr)
	{
		Destroy();
	}
}

