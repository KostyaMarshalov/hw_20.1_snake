// Fill out your copyright notice in the Description page of Project Settings.


#include "Acceleration.h"
#include "SnakeBase.h"

// Sets default values
AAcceleration::AAcceleration()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AAcceleration::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAcceleration::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAcceleration::Interact(AActor * Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AccelerationMove();
		}
	}

	if (Cast<ASnakeBase>(Interactor) != nullptr)
	{
		Destroy();
	}
}

