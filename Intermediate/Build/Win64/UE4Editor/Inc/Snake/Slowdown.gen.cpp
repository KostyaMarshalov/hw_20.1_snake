// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "Snake/Slowdown.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeSlowdown() {}
// Cross Module References
	SNAKE_API UClass* Z_Construct_UClass_ASlowdown_NoRegister();
	SNAKE_API UClass* Z_Construct_UClass_ASlowdown();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_Snake();
	SNAKE_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ASlowdown::StaticRegisterNativesASlowdown()
	{
	}
	UClass* Z_Construct_UClass_ASlowdown_NoRegister()
	{
		return ASlowdown::StaticClass();
	}
	struct Z_Construct_UClass_ASlowdown_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ASlowdown_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_Snake,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ASlowdown_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Slowdown.h" },
		{ "ModuleRelativePath", "Slowdown.h" },
	};
#endif
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ASlowdown_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ASlowdown, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ASlowdown_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ASlowdown>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ASlowdown_Statics::ClassParams = {
		&ASlowdown::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ASlowdown_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ASlowdown_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ASlowdown()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ASlowdown_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ASlowdown, 414592215);
	template<> SNAKE_API UClass* StaticClass<ASlowdown>()
	{
		return ASlowdown::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ASlowdown(Z_Construct_UClass_ASlowdown, &ASlowdown::StaticClass, TEXT("/Script/Snake"), TEXT("ASlowdown"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ASlowdown);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
